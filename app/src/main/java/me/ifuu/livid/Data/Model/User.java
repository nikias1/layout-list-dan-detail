package me.ifuu.livid.Data.Model;

public class User {

    private String uId;
    private String name;
    private String image_name;



    private String image_path;
    private String address;
    private String phone;

    public User() {
    }

    public User(String uId, String name, String image_name, String image_path, String address, String phone) {
        this.uId = uId;
        this.name = name;
        this.image_name = image_name;
        this.image_path = image_path;
        this.address = address;
        this.phone = phone;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }
}
