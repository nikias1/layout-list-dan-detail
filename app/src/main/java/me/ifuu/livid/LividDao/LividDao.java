package me.ifuu.livid.LividDao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import me.ifuu.livid.Data.Model.FavMovie;

@Dao
public interface LividDao {
    @Query("select * from FavMovie where uId = :uId")
    List<FavMovie> getAll(String uId);

    @Query("select * from FavMovie where tmdbId = :tmdbId and uId = :uId")
    FavMovie selectOneRow(String tmdbId, String uId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(FavMovie favMovie);

    @Update
    void update(FavMovie favMovie);

    @Delete
    void delete(FavMovie favMovie);
}
