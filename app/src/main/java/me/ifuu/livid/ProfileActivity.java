package me.ifuu.livid;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import me.ifuu.livid.Data.Model.User;
import me.ifuu.livid.Utils.DownloadImage;

public class ProfileActivity extends AppCompatActivity {

    //form
    @BindView(R.id.edt_name) EditText edt_name;
    @BindView(R.id.edt_phone) EditText edt_phone;
    @BindView(R.id.edt_address) EditText edt_address;

    @BindView(R.id.profile_image) CircleImageView proImg;
    @BindView(R.id.btn_save) Button btn_save;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.loading) ProgressBar loading;

    private FirebaseDatabase mDb;
    private FirebaseUser currentUser;
    private DatabaseReference databaseReference;
    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;
    private Image mImage;

    ArrayList<Image> images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        progressBar.setVisibility(View.GONE);
//        loading.setVisibility(View.GONE);
        mDb = FirebaseDatabase.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        setForm();

        getSupportActionBar().setTitle("Edit Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void endLoad(){
        progressBar.setVisibility(View.GONE);
        btn_save.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_save) void submit()
    {
        btn_save.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        updateProfile();
    }

    private void updateProfile()
    {
        final String name = edt_name.getText().toString();
        final String address = edt_address.getText().toString();
        final String phone = edt_phone.getText().toString();

        if (name.isEmpty()){
            edt_name.setError("name is required");
            edt_name.requestFocus();
            endLoad();
            return;
        }

        if (address.isEmpty()){
            edt_address.setError("address is required");
            edt_address.requestFocus();
            endLoad();
            return;
        }

        if (phone.isEmpty()){
            edt_phone.setError("phone is required");
            edt_phone.requestFocus();
            endLoad();
            return;
        }
        if (mImage != null) {
            final File file = new File(mImage.getPath());
            Uri uriFile = Uri.fromFile(file);

            final String path = "profile_images/" + file.getName();

            databaseReference = mDb.getReference("users/"+currentUser.getUid());
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User oldUser = dataSnapshot.getValue(User.class);
                    storageReference.child("profile_image/"+oldUser.getImage_name()).delete();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            storageReference.child(path).putFile(uriFile)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            storageReference.child(path).getDownloadUrl()
                                    .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            User user = new User();
                                            user.setName(edt_name.getText().toString());
                                            user.setImage_path(uri.toString());
                                            user.setPhone(edt_phone.getText().toString());
                                            user.setImage_name(file.getName());
                                            user.setAddress(edt_address.getText().toString());
                                            user.setuId(currentUser.getUid());

                                            mDb.getReference("users").child(currentUser.getUid()).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    endLoad();
                                                    Toast.makeText(ProfileActivity.this, "Profile Saved!", Toast.LENGTH_SHORT).show();
                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    endLoad();
                                                    Toast.makeText(ProfileActivity.this, "System error occurred!", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    endLoad();
                                    Toast.makeText(ProfileActivity.this, "System error occurred", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    endLoad();
                    Toast.makeText(ProfileActivity.this, "System error occurred!", Toast.LENGTH_SHORT).show();
                }
            });
        }else{

            databaseReference = mDb.getReference("users/"+currentUser.getUid());
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User oldUser = dataSnapshot.getValue(User.class);

                    User user = new User();
                    user.setName(edt_name.getText().toString());
                    user.setImage_path(oldUser.getImage_path());
                    user.setImage_name(oldUser.getImage_name());
                    user.setPhone(edt_phone.getText().toString());
                    user.setAddress(edt_address.getText().toString());
                    user.setuId(currentUser.getUid());

                    mDb.getReference("users").child(currentUser.getUid()).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            endLoad();
                            Toast.makeText(ProfileActivity.this, "Profile Saved!", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            endLoad();
                            Toast.makeText(ProfileActivity.this, "System error occurred!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }
    }

    @OnClick(R.id.profile_image) void change()
    {
        ImagePicker.with(this)               //  Initialize ImagePicker with activity or fragment context
                .setToolbarColor("#212121")         //  Toolbar color
                .setStatusBarColor("#000000")       //  StatusBar color (works with SDK >= 21  )
                .setToolbarTextColor("#FFFFFF")     //  Toolbar text color (Title and Done button)
                .setToolbarIconColor("#FFFFFF")     //  Toolbar icon color (Back and Camera button)
                .setProgressBarColor("#4CAF50")     //  ProgressBar color
                .setBackgroundColor("#212121")      //  Background color
                .setCameraOnly(false)               //  Camera mode
                .setMultipleMode(false)              //  Select multiple images or single image
                .setFolderMode(true)                //  Folder mode
                .setShowCamera(true)                //  Show camera button
                .setFolderTitle("Albums")           //  Folder title (works with FolderMode = true)
                .setImageTitle("Galleries")         //  Image title (works with FolderMode = false)
                .setDoneTitle("Done")               //  Done button title
                .setLimitMessage("You have reached selection limit")    // Selection limit message
                .setMaxSize(10)                     //  Max images can be selected
                .setSavePath("ImagePicker")         //  Image capture folder name
                .setSelectedImages(images)          //  Selected images
                .setAlwaysShowDoneButton(true)      //  Set always show done button in multiple mode
                .setKeepScreenOn(true)              //  Keep screen on when selecting images
                .start();                           //  Start ImagePicker
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbarProfile:
                showDialogMessage();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showDialogMessage()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure want to log out ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                FirebaseAuth.getInstance().signOut();
                finish();
                Toast.makeText(ProfileActivity.this, "Goodbye :)", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(ProfileActivity.this, "cancelled", Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setForm()
    {
        loading.setVisibility(View.VISIBLE);
        databaseReference = mDb.getReference("users/"+currentUser.getUid());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user.getImage_path().equals("example/path")){
                    proImg.setImageResource(R.drawable.ic_image);
                }else{
                    DownloadImage.picassoProfile(user.getImage_path(), proImg);
                }
                edt_name.setText(user.getName());
                edt_address.setText(user.getAddress());
                edt_phone.setText(user.getPhone());
                loading.setVisibility(View.GONE);
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
                int FIRST_IMAGE = 0;
            Bitmap bitmap = BitmapFactory.decodeFile(images.get(FIRST_IMAGE).getPath());
            mImage = images.get(FIRST_IMAGE);
            proImg.setImageBitmap(bitmap);
        }
        super.onActivityResult(requestCode, resultCode, data);  // You MUST have this line to be here
        // so ImagePicker can work with fragment mode
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }
}
