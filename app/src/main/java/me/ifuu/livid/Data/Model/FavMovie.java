package me.ifuu.livid.Data.Model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class FavMovie {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "uId")
    private String uId;

    @ColumnInfo(name = "tmdbId")
    private String tmdbId;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "image")
    private String img;


    public FavMovie() {
    }

    public String getUId() {
        return uId;
    }

    public void setUId(String uId) {
        this.uId = uId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
