package me.ifuu.livid;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.ifuu.livid.Data.Model.User;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.logo) TextView txt_logo;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.btn_login) Button btn_login;
    //firebase
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDb;

    //form
    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.edt_password) EditText edt_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        checkSession();
        progressBar.setVisibility(View.GONE);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Nabila.ttf");
        txt_logo.setTypeface(face);
        mAuth = FirebaseAuth.getInstance();
        mDb = FirebaseDatabase.getInstance();

        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @OnClick(R.id.txt_register) void click(){
        Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_login) void submit (){
        btn_login.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        login();
    }

    private void login(){
        String email = edt_email.getText().toString();
        String password = edt_password.getText().toString();

        if (email.isEmpty()){
            edt_email.setError("Email is required");
            edt_email.requestFocus();
            endLoad();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            edt_email.setError("Please enter a valid email");
            edt_email.requestFocus();
            endLoad();
            return;
        }

        if (password.isEmpty()){
            edt_password.setError("Password is required");
            edt_password.requestFocus();
            endLoad();
            return;
        }

        if (password.length()<6){
            edt_password.setError("Minimum lenght of password should be 6");
            edt_password.requestFocus();
            endLoad();
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                mDb.getReference("users/"+authResult.getUser().getUid()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        finish();
                        Toast.makeText(LoginActivity.this, "Welcome "+user.getName(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                endLoad();
                Toast.makeText(LoginActivity.this, "Login Failed! check your email and password", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkSession();
    }

    private void checkSession(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null){
            finish();
        }
    }

    private void endLoad(){
        progressBar.setVisibility(View.GONE);
        btn_login.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish(); // untuk menutup actifity
        return true;
    }
}
