package me.ifuu.livid.Data;

public class Api {
    private static String KEY = "3c45f7c915f6ab44988c9adc23245203";
    private static String BASE_URL = "http://api.themoviedb.org/3/";
    public static String MOVIE_POPULAR(String page)
    {
        return BASE_URL+"movie/popular?api_key="+KEY+"&language=en-US&page="+page;
    }
    public static String MOVIE_UPCOMMING(String page)
    {
        return BASE_URL+"movie/upcoming?api_key="+KEY+"&language=en-US&page="+page;
    }
    public static String POSTER_PATH = "http://image.tmdb.org/t/p/w600_and_h900_bestv2";


    public static String TRAILER (String id)
    {
        return BASE_URL+"movie/"+id+"/videos?api_key="+KEY+"&language=en-US";
    }

    public static String URL_YOUTUBE(String key)
    {
        return "http://www.youtube.com/watch?v="+key;
    }

    public static String Detail (String id)
    {
        return BASE_URL+"movie/"+id+"?api_key="+KEY+"&language=en-US";
    }
}
