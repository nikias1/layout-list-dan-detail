package me.ifuu.livid;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.ifuu.livid.Adapter.CommentAdapter;
import me.ifuu.livid.Data.Model.Comment;
import me.ifuu.livid.Data.Model.User;
import me.ifuu.livid.Utils.DownloadImage;

import static android.support.v7.widget.RecyclerView.*;

public class CommentActivity extends AppCompatActivity {

    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.rcvComment) RecyclerView mRcv;
    @BindView(R.id.edt_message) EditText edt_comment;
    private DatabaseReference databaseReference;
    private FirebaseUser currentUser;
    private FirebaseDatabase mDb;
    private CommentAdapter mAdapter;

    ArrayList<Comment> comments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);
        mDb = FirebaseDatabase.getInstance();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        progressBar.setVisibility(View.GONE);

        displayComment();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.btn_send) void submit()
    {
        sendComment();
    }

    public void displayComment()
    {
        progressBar.setVisibility(View.VISIBLE);
        Bundle bundle = getIntent().getExtras();
        String id = bundle.getString("_id");
        databaseReference = mDb.getReference("comment/");
        Query commentQuery = databaseReference.orderByChild("tmdbId").equalTo(id);
        commentQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (comments != null){
                    comments.clear();
                }
                for (DataSnapshot COMMENTS : dataSnapshot.getChildren()){
                    if (COMMENTS != null) {

                        Comment comment = new Comment();
                        comment.setuId(COMMENTS.child("uId").getValue(String.class));
                        comment.setKey(COMMENTS.child("key").getValue(String.class));
                        comment.setName(COMMENTS.child("name").getValue(String.class));
                        comment.setTmdbId(COMMENTS.child("tmdbId").getValue(String.class));
                        comment.setOnCreate(COMMENTS.child("onCreate").getValue(String.class));
                        comment.setImage_path(COMMENTS.child("image_path").getValue(String.class));
                        comment.setComment(COMMENTS.child("comment").getValue(String.class));

                        comments.add(comment);

                    }
                }
                    if (comments != null){
                        mRcv.setHasFixedSize(true);
                        mAdapter = new CommentAdapter(comments);
                        LinearLayoutManager linear = new LinearLayoutManager(getApplicationContext());
                        linear.setReverseLayout(true);
                        linear.setStackFromEnd(true);
                        mRcv.setLayoutManager(linear);
                        mRcv.setAdapter(mAdapter);

                    }


                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void sendComment()
    {
        if (currentUser != null) {

            final String message = edt_comment.getText().toString();

            if (message.isEmpty()) {
                edt_comment.setError("Comment Required!");
                edt_comment.requestFocus();
                return;
            }

            final String key = mDb.getReference("users/" + currentUser.getUid()).push().getKey();

            databaseReference = mDb.getReference("users/" + currentUser.getUid());
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);

                    Comment comment = new Comment();
                    comment.setuId(currentUser.getUid());
                    comment.setKey(key);
                    comment.setName(user.getName());
                    comment.setComment(message);
                    comment.setImage_path(user.getImage_path());
                    Bundle bundle = getIntent().getExtras();
                    String id = bundle.getString("_id");
                    comment.setTmdbId(id);
                    Date date = Calendar.getInstance().getTime();
                    DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
                    String currentTime = dateFormat.format(date);
                    comment.setOnCreate(currentTime);

                    mDb.getReference("comment/" + key).setValue(comment).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            edt_comment.setText("");
                            Toast.makeText(CommentActivity.this, "Sent", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            edt_comment.setText("");
                            Toast.makeText(CommentActivity.this, "failed sent!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }


                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }else {
            showDialogMessage();
        }
    }

    public void showDialogMessage()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(CommentActivity.this);
        builder.setTitle("error");
        builder.setMessage("to add comment you must Login first, do you want Login now ? .");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(), "cancelled", Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
    }
}
