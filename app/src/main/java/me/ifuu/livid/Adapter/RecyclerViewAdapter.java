package me.ifuu.livid.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import me.ifuu.livid.Data.Api;
import me.ifuu.livid.Holder.RecyclerViewHolder;
import me.ifuu.livid.Data.Model.Movie;
import me.ifuu.livid.R;
import me.ifuu.livid.Utils.DownloadImage;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private ArrayList<Movie>  listMovie;
    private ListItemListener mListener;

    public RecyclerViewAdapter(ArrayList<Movie> listMovie) { this.listMovie = listMovie; }

    public void setOnItemClickListener(ListItemListener listener)
    {
        mListener = listener;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_movie, parent, false);

        RecyclerViewHolder rvh = new RecyclerViewHolder(view, mListener);
        return rvh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final Movie movie = listMovie.get(position);
        // menampilkan data
        DownloadImage.picasso(Api.POSTER_PATH + movie.getImg(), holder.mPoster);
        holder.mTitle.setText(movie.getTitle());

    }

    @Override
    public int getItemCount() { return listMovie.size(); }

    public interface ListItemListener { void OnItemClicked(int position); }
}