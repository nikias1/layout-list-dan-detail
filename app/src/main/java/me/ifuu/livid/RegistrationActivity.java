package me.ifuu.livid;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.ifuu.livid.Data.Model.User;

public class RegistrationActivity extends AppCompatActivity {


    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.logo) TextView txt_logo;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.btn_reg) Button btn_reg;
    //firebase
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDb;

    // form
    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.edt_password) EditText edt_password;
    @BindView(R.id.edt_name) EditText edt_name;
    @BindView(R.id.edt_phone) EditText edt_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Nabila.ttf");
        txt_logo.setTypeface(face);
        //inisialitation
        progressBar.setVisibility(View.GONE);
        mAuth = FirebaseAuth.getInstance();
        mDb = FirebaseDatabase.getInstance();

        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @OnClick(R.id.txt_login) void click(){
        finish();
    }

    @OnClick(R.id.btn_reg) void submit (){
        progressBar.setVisibility(View.VISIBLE);
        btn_reg.setVisibility(View.GONE);
        registerUser();
    }

    public void registerUser(){
        String email = edt_email.getText().toString();
        String password = edt_password.getText().toString();
        final String name = edt_name.getText().toString();
        final String phone = edt_phone.getText().toString();

        if (email.isEmpty()){
            edt_email.setError("Email is required");
            edt_email.requestFocus();
            endLoad();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            edt_email.setError("Please enter a valid email");
            edt_email.requestFocus();
            endLoad();
            return;
        }

        if (password.isEmpty()){
            edt_password.setError("Password is required");
            edt_password.requestFocus();
            endLoad();
            return;
        }

        if (password.length()<6){
            edt_password.setError("Minimum lenght of password should be 6");
            edt_password.requestFocus();
            endLoad();
            return;
        }

        if (name.isEmpty()){
            edt_name.setError("Password is required");
            edt_name.requestFocus();
            endLoad();
            return;
        }

        if (phone.isEmpty()){
            edt_phone.setError("Password is required");
            edt_phone.requestFocus();
            endLoad();
            return;
        }

        mAuth.createUserWithEmailAndPassword(email, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                FirebaseUser fbUser = authResult.getUser();

                User newUser = new User();
                newUser.setuId(fbUser.getUid());
                newUser.setName(name);
                newUser.setPhone(phone);
                newUser.setImage_name("example/name");
                newUser.setAddress("example/address");
                newUser.setImage_path("example/path");

                mDb.getReference("users/"+ fbUser.getUid()).setValue(newUser)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        finish();
                        Toast.makeText(RegistrationActivity.this, "Welcome "+name, Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        endLoad();
                        Toast.makeText(RegistrationActivity.this, "Registration failed!", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                endLoad();
                Toast.makeText(RegistrationActivity.this, "Registration Failed! System error occurred", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void endLoad(){
        progressBar.setVisibility(View.GONE);
        btn_reg.setVisibility(View.VISIBLE);
    }


    @Override
    public boolean onSupportNavigateUp(){
        finish(); // untuk menutup actifity
        return true;
    }
}
