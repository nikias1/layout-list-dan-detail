package me.ifuu.livid.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import me.ifuu.livid.Data.Model.Comment;
import me.ifuu.livid.Holder.CommentHolder;
import me.ifuu.livid.R;
import me.ifuu.livid.Utils.DownloadImage;

public class CommentAdapter extends RecyclerView.Adapter<CommentHolder>{
    ArrayList<Comment> mComment;
    ListItemListener mListener;

    public CommentAdapter(ArrayList<Comment> mComment) {
        this.mComment = mComment;
    }


    public void setOnItemClickListener(ListItemListener listener)
    {
        mListener = listener;
    }

    @NonNull
    @Override
    public CommentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_movie, parent, false);

        CommentHolder rvh = new CommentHolder(view, mListener);
        return rvh;
    }

    @Override
    public void onBindViewHolder(@NonNull CommentHolder holder, int position) {
        final Comment comment = mComment.get(position);

        DownloadImage.picassoComment(comment.getImage_path(), holder.mImage);
        holder.mName.setText(comment.getName());
        holder.mComment.setText(comment.getComment());
        holder.mDate.setText(comment.getOnCreate());
    }

    @Override
    public int getItemCount() {
        return mComment.size();
    }

    public interface ListItemListener { void OnItemCliked(int position); }
}
