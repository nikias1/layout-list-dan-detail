package me.ifuu.livid.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import me.ifuu.livid.Data.Model.TrailerMovie;
import me.ifuu.livid.R;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {
    private ArrayList<TrailerMovie> mListTrailerMovie;
    private OnItemClickListener mListener;

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener)
    {
        mListener = listener;
    }

    public static class ListViewHolder extends RecyclerView.ViewHolder{
        public ImageView mImageView;
        public TextView mTitle;
        public TextView mSite;


        public ListViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.Icon);
            mTitle = itemView.findViewById(R.id.Title);
            mSite = itemView.findViewById(R.id.Site);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null)
                    {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION)
                        {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public ListAdapter(ArrayList<TrailerMovie> trailerMovieList){ mListTrailerMovie = trailerMovieList; }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trailer_movie, parent, false);
        // lvh = list view holder
        ListViewHolder lvh = new ListViewHolder(view, mListener);
        return lvh;
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position)
    {
        TrailerMovie currentTrailerMovie = mListTrailerMovie.get(position);

        holder.mImageView.setImageResource(currentTrailerMovie.getmImageResource());
        holder.mTitle.setText(currentTrailerMovie.getmTitle());
        holder.mSite.setText(currentTrailerMovie.getmSite());
    }

    @Override
    public int getItemCount(){ return mListTrailerMovie.size(); }


}
