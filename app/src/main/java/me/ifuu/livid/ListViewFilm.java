package me.ifuu.livid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import me.ifuu.livid.Adapter.ListAdapter;
import me.ifuu.livid.Data.Model.TrailerMovie;

public class ListViewFilm extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private ListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    final ArrayList<TrailerMovie> trailerMovieList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_film);

        addItem();
        buildRecyclerView();
    }

    public void addItem()
    {
        trailerMovieList.add(new TrailerMovie(R.drawable.ic_android, "awdwd", "asdsad"));
        trailerMovieList.add(new TrailerMovie(R.drawable.ic_adb, "film b", "dec b"));
        trailerMovieList.add(new TrailerMovie(R.drawable.ic_build, "film c    ", "desc c"));
    }

    public void actionClick(int position)
    {
        Intent intent = new Intent(ListViewFilm.this, DetailFilm.class);
        intent.putExtra("data_position", position);
        startActivity(intent);

//        trailerMovieList.get(position).clikedItem(trailerMovieList.get(position).getmDesc());
//        mAdapter.notifyItemChanged(position);
    }

    public void buildRecyclerView()
    {
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ListAdapter(trailerMovieList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new ListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                actionClick(position);
            }
        });
    }


}
