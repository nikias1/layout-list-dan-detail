package me.ifuu.livid.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import me.ifuu.livid.Data.Api;
import me.ifuu.livid.Data.Model.FavMovie;
import me.ifuu.livid.Data.Model.Movie;
import me.ifuu.livid.Holder.FavoriteHolder;
import me.ifuu.livid.Holder.RecyclerViewHolder;
import me.ifuu.livid.R;
import me.ifuu.livid.Utils.DownloadImage;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteHolder>{
    private List<FavMovie> listMovie;
    private ListItemListener mListener;

    public FavoriteAdapter(ListItemListener mListener) {
        this.mListener = mListener;
    }

    public FavoriteAdapter(List<FavMovie> listMovie) {
        this.listMovie = listMovie;
    }

    public void setOnItemClickListener(ListItemListener listener)
    {
        mListener = listener;
    }

    @NonNull
    @Override
    public FavoriteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_movie, parent, false);

        FavoriteHolder rvh = new FavoriteHolder(view, mListener);
        return rvh;
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteHolder holder, int position) {
        final FavMovie movie = listMovie.get(position);
        // menampilkan data
        DownloadImage.picasso(Api.POSTER_PATH + movie.getImg(), holder.mPoster);
        holder.mTitle.setText(movie.getTitle());

    }

    @Override
    public int getItemCount() { return listMovie.size(); }

    public interface ListItemListener { void OnItemClicked(int position); }
}
