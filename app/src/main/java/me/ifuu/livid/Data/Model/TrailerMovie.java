package me.ifuu.livid.Data.Model;

public class TrailerMovie {
    private int mImageResource, id;
    private String mTitle;
    private String mKey;
    private String mSite;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TrailerMovie(int mImageResource, int id, String mTitle, String mKey, String mSite) {

        this.mImageResource = mImageResource;
        this.id = id;
        this.mTitle = mTitle;
        this.mKey = mKey;
        this.mSite = mSite;
    }

    public TrailerMovie(int mImageResource, String mTitle, String mKey) {
        this.mImageResource = mImageResource;
        this.mTitle = mTitle;
        this.mKey = mKey;
    }

    public TrailerMovie() {
    }

    public int getmImageResource() {
        return mImageResource;
    }


    public String getmTitle() {
        return mTitle;
    }

    public void setmImageResource(int mImageResource) {
        this.mImageResource = mImageResource;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmKey() {
        return mKey;
    }

    public void setmKey(String mKey) {
        this.mKey = mKey;
    }

    public String getmSite() {
        return mSite;
    }

    public void setmSite(String mSite) {
        this.mSite = mSite;
    }
}
