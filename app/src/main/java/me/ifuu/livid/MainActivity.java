package me.ifuu.livid;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.txtSplash) TextView txt_splash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Nabila.ttf");
        txt_splash.setTypeface(face);

        Handler handler = new Handler();
        Runnable runnable = (new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, MainTabs.class);
                startActivity(intent);
                finish();
            }
        });

        handler.postDelayed(runnable, 2000);
    }
}
