package me.ifuu.livid.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import me.ifuu.livid.Adapter.FavoriteAdapter;
import me.ifuu.livid.Adapter.RecyclerViewAdapter;
import me.ifuu.livid.Data.Model.FavMovie;
import me.ifuu.livid.Data.Model.Movie;
import me.ifuu.livid.DetailFilm;
import me.ifuu.livid.MyApp;
import me.ifuu.livid.R;


public class FavoriteMovie extends Fragment {

    private RecyclerView mRcv;
    private ProgressBar progressBar;
    private FavoriteAdapter mAdapter;
    // Required empty public constructor
    private RecyclerView.LayoutManager gridLayoutManager;
    private FirebaseUser currentUser;

    List<FavMovie> movie;

    public FavoriteMovie() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite_movie, container, false);
        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        mRcv = view.findViewById(R.id.rcvFavorite);
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);

        if (currentUser != null) {
            display();
        }

        return view;
    }

    public void display()
    {
//        mRcv.setVisibility(View.VISIBLE);

        //display a progressbar
        progressBar.setVisibility(View.VISIBLE);

        //refresh or reset recycler view and array list
        mRcv.setAdapter(null);

        movie = MyApp.db.lividDao().getAll(currentUser.getUid());

        mAdapter = new FavoriteAdapter(movie);
        mRcv.setLayoutManager(gridLayoutManager);
        mRcv.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new FavoriteAdapter.ListItemListener() {
            @Override
            public void OnItemClicked(int position) {
                actionClick(position);
            }
        });

        // hidden a progressbar
        progressBar.setVisibility(View.GONE);

    }

    public void actionClick(int position)
    {
        Intent intent = new Intent(getActivity(), DetailFilm.class);
        int id = Integer.parseInt(movie.get(position).getTmdbId());
        String title = movie.get(position).getTitle();
        intent.putExtra("_id", id);
        intent.putExtra("_title", title);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            display();
        }else{
            if (movie != null) {
                progressBar.setVisibility(View.VISIBLE);
                movie.clear();
                mRcv.setAdapter(null);
                progressBar.setVisibility(View.GONE);
            }
        }
    }
}