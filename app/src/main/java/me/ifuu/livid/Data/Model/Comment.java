package me.ifuu.livid.Data.Model;

import java.util.Date;

public class Comment {
    private String uId;
    private String key;
    private String name;
    private String comment;
    private String image_path;
    private String onCreate;
    private String tmdbId;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Comment(String uId, String name, String comment, String image_path, String tmdbId, String key) {
        this.uId = uId;
        this.name = name;
        this.comment = comment;
        this.image_path = image_path;
        this.tmdbId = tmdbId;
        this.onCreate = String.valueOf(new Date().getTime());
        this.key = key;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getOnCreate() {
        return onCreate;
    }

    public void setOnCreate(String onCreate) {
        this.onCreate = onCreate;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public Comment() {

    }
}
