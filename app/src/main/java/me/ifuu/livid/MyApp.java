package me.ifuu.livid;

import android.app.Application;
import android.arch.persistence.room.Room;

import me.ifuu.livid.Database.AppRoomDatabase;

public class MyApp extends Application {
    public static AppRoomDatabase db;

    @Override
    public void onCreate() {
        super.onCreate();

        db = Room.databaseBuilder(getApplicationContext(),
                AppRoomDatabase.class,
                "favoritemovie.db").allowMainThreadQueries().build();
    }
}
