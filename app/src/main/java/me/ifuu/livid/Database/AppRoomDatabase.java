package me.ifuu.livid.Database;

import android.app.Application;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import me.ifuu.livid.Data.Model.FavMovie;
import me.ifuu.livid.LividDao.LividDao;

// If You Change a Database, Don't forget to change version too
@Database(entities = {FavMovie.class}, version = 1)
public abstract class AppRoomDatabase extends RoomDatabase {
    public  abstract LividDao lividDao();
}
